
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  PanResponder,
  Dimensions,
  Animated
} from 'react-native';
import Colors from "./Colors";
import Screen from "./Screen";

const SWIPE_THRESHOLD = 0.2 * Screen.height;
const SWIPE_SPEED_THRESHOLD = 0.3;
const SWIPE_OUT_DURATION = 250;

export default class Modal extends Component {

  constructor(props) {
    super(props);

    this.position = new Animated.Value(Screen.height);
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (e, gestureState) => {
        this.position.setValue(gestureState.dy);
      },
      onPanResponderRelease: (e, gestureState) => {
        if (gestureState.vy > SWIPE_SPEED_THRESHOLD || gestureState.dy > SWIPE_THRESHOLD) {
          this.hide();
        }
        else if (gestureState.dy > 0) {
          this.resetPosition();
        }
      },
    });

    this.state = {
      visible: false,
    }

  }

  show = () => {
    this.setState({ visible: true });
    Animated.spring(this.position, {
      toValue: 0,
      useNativeDriver: true
    }).start();
  }


  hide = () => {
    Animated.timing(this.position, {
      toValue: Screen.height,
      useNativeDriver: true,
      duration: SWIPE_OUT_DURATION
    }).start(() => this.setState({ visible: false }));
  }

  resetPosition = () => {
    Animated.spring(this.position, {
      toValue: 0,
      useNativeDriver: true
    }).start();
  }

  render() {
    return (
      this.state.visible
        ?
        <View style={styles.outerContainer} >
          <Animated.View
            style={{
              ...styles.background,
              opacity: this.position.interpolate({
                inputRange: [0, Screen.height / 2, Screen.height],
                outputRange: [0.5, 0.1, 0],
                extrapolate: "clamp"
              })
            }}
          />
          <Animated.View
            {...this.panResponder.panHandlers}
            style={{
              ...styles.innerContainer,
              transform: [{
                translateY: this.position.interpolate({
                  inputRange: [0, Screen.height],
                  outputRange: [0, Screen.height],
                  extrapolate: "clamp"
                })
              }]
            }}
          >
            <View style={{ backgroundColor: Colors.primaryDark, borderRadius: 5, width: 80, height: 5, marginVertical: 10 }} />
            {this.props.children}
          </Animated.View>
        </View>
        :
        <View />

    );
  }


}

const styles = {
  outerContainer: {
    position: "absolute",
    zIndex: 10,
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: "flex-end",
  },
  innerContainer: {
    zIndex: 12,
    backgroundColor: Colors.primary,
    width: Screen.width,
    alignItems: "center",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingBottom: 10,
    transform: [{
      translateY: 100
    }],
  },
  background: {
    position: "absolute",
    zIndex: 11,
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: "center",
    backgroundColor: "black",
  },
  button: {
    marginTop: 80,
    backgroundColor: Colors.secondary,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 5
  },
  textDefault: {
    fontSize: 14,
    color: "black"
  },
  textInverted: {
    fontSize: 14,
    color: Colors.primary
  },

};