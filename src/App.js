
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Colors from "./Colors";
import Modal from "./Modal";
import { Picker, Icon } from "native-base";
import { DatePicker } from 'react-native-wheel-picker-android'
var moment = require("moment-timezone");

export default class App extends Component {


  constructor(props) {
    super(props);
    this.datePickerActive = true;

    this.fromDate = moment().format();
    this.toDate = moment().format();
    this.timeZones = moment.tz.names();

    this.state = {
      selectedTimezone: moment.tz.guess(),
      date: new Date(),
    };

  }

  showModal = () => {
    this.modalRef.show();
  }

  submit = () => {
    if (moment(this.fromDate).isAfter(moment(this.toDate))) {
      alert("From date cannot be later than to date");
      return;
    }
    alert("Generated and logged JSON data!");
    var jsonData = JSON.stringify({
      timezone: this.state.selectedTimezone,
      from: this.fromDate,
      to: this.toDate
    }, undefined, 2);
    console.log(jsonData);
    this.modalRef.hide();
  }

  renderTimeZonePicker = () => {
    return (
      <Picker
        style={{ width: "100%", borderWidth: 1, borderColor: Colors.primaryDark, borderRadius: 5 }}
        iosIcon={<Icon name="arrow-down" style={{ position: "absolute", right: 0 }} />}
        selectedValue={this.state.selectedTimezone}
        onValueChange={(itemValue) => {
          this.setState({
            selectedTimezone: itemValue,
          });
        }}
      >
        {this.timeZones.map(tz => {
          return (
            <Picker.Item key={tz} label={`${tz} (${moment.tz(tz).zoneName()})`} value={tz} />
          )
        })}
      </Picker>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={{ ...styles.button, marginTop: 80 }} onPress={this.showModal} >
          <Text style={styles.textInverted} >Show Modal</Text>
        </TouchableOpacity>



        <Modal ref={r => this.modalRef = r} >
          <View style={{ width: Screen.width * 0.88, alignItems: "center", justifyContent: "center" }} >

            <Text style={{ ...styles.textHeader, width: "100%" }} >I'm not available until</Text>
            {Platform.OS == "android" ?
              <View style={{ width: "100%", borderWidth: 1, borderColor: Colors.primaryDark, borderRadius: 5 }} >
                {this.renderTimeZonePicker()}
              </View>
              :
              this.renderTimeZonePicker()
            }

            <Text style={{ ...styles.textHeader, width: "100%", marginTop: 20 }} >From</Text>
            <View style={styles.datePickerContainer} >
              <TouchableWithoutFeedback  >
                {/* Androidde PanHandler ile DatePicker arasındaki öncelik sorunununu TouchableWithoutFeedback ile wraplayarak çözdüm */}
                <DatePicker
                  onDateSelected={(date) => { this.fromDate = moment(date).format() }}
                  minimumDate={this.state.date}
                  mode="datetime"
                  minuteInterval={15}
                  minutes={["00", "15", "30", "45"]}
                />
              </TouchableWithoutFeedback>
            </View>

            <Text style={{ ...styles.textHeader, width: "100%", marginTop: 20 }} >To</Text>
            <View style={styles.datePickerContainer} >
              <TouchableWithoutFeedback  >
                <DatePicker
                  onDateSelected={(date) => { this.toDate = moment(date).format() }}
                  minimumDate={this.state.date}
                  mode="datetime"
                  minuteInterval={15}
                  minutes={["00", "15", "30", "45"]}
                />
              </TouchableWithoutFeedback>
            </View>

            <TouchableOpacity onPress={this.submit} style={{ ...styles.button, width: "100%", marginTop: 10 }} >
              <Text style={styles.textInverted} >Submit</Text>
            </TouchableOpacity>


          </View>
        </Modal>

      </View >
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  button: {
    backgroundColor: Colors.secondary,
    paddingVertical: 12,
    alignItems: "center",
    width: Screen.width * 0.8,
    borderRadius: 5
  },
  textDefault: {
    fontSize: 18,
    color: "black",
  },
  textHeader: {
    fontSize: 18,
    fontWeight: "500",
    color: "black",
    marginBottom: 10
  },
  textInverted: {
    fontSize: 18,
    color: Colors.primary
  },
  datePickerContainer: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.primaryDark,
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    paddingTop: Platform.OS == "android" ? 40 : 0,
    overflow: "hidden",
    width: "100%"
  }

});
