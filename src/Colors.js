export default Colors = {
    primary: "#F5FCFF",
    primaryDark: "#dcdcdc",
    secondary: "#1043b4"
};