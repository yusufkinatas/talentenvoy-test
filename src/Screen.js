import {
    Dimensions
} from 'react-native';

export default Screen = {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
}